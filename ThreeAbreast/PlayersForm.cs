﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    public class PlayersForm : Form
    {
        private Queue<Action> animationEventQueue = new Queue<Action>();


        public PlayersForm(Board board)
        {
            var animationTimer = new System.Windows.Forms.Timer { Interval = 100 };
            var firstPlayerPanel = new PlayerPanelPlus("MainCharacter");
            var secondPlayerPanel = new PlayerPanelPlus("FirstEnemy");
            animationTimer.Tick += firstPlayerPanel.TimerTick;
            animationTimer.Tick += secondPlayerPanel.TimerTick;

            var table = new TableLayoutPanel();
            table.RowStyles.Add(
                new RowStyle(SizeType.Percent, 100f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 50f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 50f));

            table.Controls.Add(firstPlayerPanel, 0, 0);
            table.Controls.Add(secondPlayerPanel, 1, 0);
            table.Dock = DockStyle.Fill;

            Controls.Add(table);

            animationTimer.Tick += (sender, args) =>
            {
                if (animationEventQueue.Count > 0)
                    animationEventQueue.Dequeue()();
            };
            Dock = DockStyle.Fill;
            WindowState = FormWindowState.Maximized;
            animationTimer.Start();
        }
    }
}
