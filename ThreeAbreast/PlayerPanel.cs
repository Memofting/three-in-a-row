﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    class PlayerPanel: Panel
    {
        private Label label;
        private TableLayoutPanel table;

        public PlayerPanel(string name)
        {
            label = new Label();
            label.Text = name;
            label.Font = new Font("Arial", 16);

            table = new TableLayoutPanel();
            table.RowStyles.Add(
                new RowStyle(SizeType.Percent, 100f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 100f));
            table.Controls.Add(label, 0, 0);

            Dock = DockStyle.Fill;
            Controls.Add(table);
        }
    }
}
