﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ThreeAbreast
{
    static class BoardLoader
    {
        static public char[] Separators = new char[] { 'x', ';' }; 

        static public Board Load(string name)
        {
            var boardPath = Path.Combine("Boards", name);
            var absBoardPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location), 
                Path.Combine("Boards", name));
            if (!File.Exists(absBoardPath))
                throw new FileNotFoundException();
            var lines = File.ReadAllLines(absBoardPath);

            return MakeBoard(lines);
        }

        static public Board MakeBoard(string[] boardInfo)
        {
            var (h, w, fillEmpty) = GetBaseBoardInfo(boardInfo[0]);
            var board = new Board(h, w);

            for (var y = 0; y < boardInfo.Length - 1; y++)
            {
                var vertical = boardInfo[y + 1].Split();
                for (var x = 0; x < vertical.Length; x++)
                    board[x, y] = CreateItem(x, y, vertical[x], fillEmpty);
            }

            return board;
        }

        static public Board MakeBoard(string boardInfo)
        {
            return MakeBoard(PrepareBoardInfo(boardInfo));
        }

        static public string[] PrepareBoardInfo(string boardInfo)
        {
            return boardInfo.Split('\n')
                .Select(line => line.TrimEnd().TrimStart())
                .Where(line => line.Length > 0)
                .ToArray();
        }

        static (int, int, bool) GetBaseBoardInfo(string header)
        {
            var boardInfo = header.Split(Separators);

            var height = Int32.Parse(boardInfo[0]);
            var width = Int32.Parse(boardInfo[1]);
            var fillEmpty = false;

            /* Если появится больше опций, эти флажки можно в отдельный класс
             запихнуть, чтобы не таскать кучу булеанов */
            if (boardInfo.Length > 2)
                if (boardInfo[2] == "FillEmpty")
                    fillEmpty = true;

            return (height, width, fillEmpty);
        }

        static BoardItem CreateItem(int x, int y, string icon, bool fillEmpty)
        {
            var itemType = BoardItem.IconToType[icon];
            if (fillEmpty && itemType == ItemType.Empty)
                itemType = BoardItem.GetRandomValidItemType();

            return new BoardItem(new Point(x, y), itemType);
        }
    }
}
