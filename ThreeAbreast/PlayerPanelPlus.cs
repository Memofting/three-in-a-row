﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    class PlayerPanelPlus : Panel
    {
        static private char[] mainSeparator = new char[] { ':' };
        static private char[] phrasesSeparator = new char[] { ';' };
        static private string defaultPhrase = "Может быть в другой раз...";
        static private string emptytPhrase = "";
        static private int lowBorder = 3;
        static private int middleBorder = 5;
        static private int hightBorder = 6;
        
        private Label portait;
        private Panel scales;
        private PhrasesBox phrasesBox;
        private PlayerTable table;
        private string name;
        private Dictionary<int, Bitmap> idleFrames = new Dictionary<int, Bitmap>();
        private Dictionary<string, List<string>> phrases = new Dictionary<string, List<string>>();
        private Random randomIntGenerator = new Random();
        private int currentFrame = 1;
        private int k = 5;

        public PlayerPanelPlus(string name)
        {
            this.name = name;
            LoadPhrases();
            LoadIdle(new DirectoryInfo($"Animation\\{name}\\Idle"));

            portait = new Label();
            portait.Dock = DockStyle.Fill;
            portait.Image = idleFrames[currentFrame];
            portait.BackColor = Color.Transparent;

            scales = new Panel();
            scales.Dock = DockStyle.Fill;
            scales.BackColor = Color.Transparent;

            phrasesBox = new PhrasesBox();
            table = new PlayerTable(portait, scales, phrasesBox);

            BackColor = Color.Transparent;
            BackgroundImage = GetBackgroundImage();
            BackgroundImageLayout = ImageLayout.None;
            Dock = DockStyle.Fill;
            Controls.Add(table);
        }

        private void LoadIdle(DirectoryInfo idleDirectory)
        {
            foreach (var e in idleDirectory.GetFiles("*.png"))
            {
                var tempImage = (Bitmap)Image.FromFile(e.FullName);
                var resizedImage = new Bitmap(tempImage.Width * k, tempImage.Height * k);
                Graphics g = Graphics.FromImage(resizedImage);
                g.InterpolationMode = InterpolationMode.NearestNeighbor;

                g.DrawImage(tempImage, 0, 0, resizedImage.Width, resizedImage.Height);
                g.Dispose();

                idleFrames[Int32.Parse(Path.GetFileNameWithoutExtension(e.Name))] =
                    resizedImage;
            }
        }

        private Image GetBackgroundImage()
        {
            var backgroundPath = Path.Combine("Images", "PlayerBackgrounds", name, "background.png");
            var absBackgroundPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                backgroundPath);
            var backImage = Image.FromFile(absBackgroundPath);
            return new Bitmap(backImage, new Size(Width * k, Height * k));
        }

        private string GetMessageFromScore(
            int AScore, int BScore, int CScore, int DScore)
        {
            var possiblePhrases = new List<string>();
            if (AScore + BScore + CScore + DScore >= hightBorder)
                return phrases["OVERSIZE"][0];
            possiblePhrases.Add(GetMessage(AScore, "A"));
            possiblePhrases.Add(GetMessage(BScore, "B"));
            possiblePhrases.Add(GetMessage(CScore, "C"));
            possiblePhrases.Add(GetMessage(DScore, "D"));
            return possiblePhrases
                        .Where(item => item != emptytPhrase)
                        .DefaultIfEmpty(defaultPhrase)
                        .OrderBy(item => randomIntGenerator.Next(hightBorder))
                        .Last();
        }

        private string GetMessage(int score, string key)
        {
            if (score >= middleBorder)
                return phrases[key][1];
            else if (score >= lowBorder)
                return phrases[key][0];
            return emptytPhrase;
        }

        public void SetMessageFromScore(
            int AScore, int BScore, int CScore, int DScore)
        {
            phrasesBox.Text = GetMessageFromScore(
                AScore, BScore, CScore, DScore);
        }

        public void TimerTick(object sender, EventArgs args)
        {
            currentFrame++;
            if (currentFrame > idleFrames.Count)
                currentFrame = 1;
            portait.Image = idleFrames[currentFrame % idleFrames.Count + 1];
        }

        private void LoadPhrases()
        {
            var phrasesPath = Path.Combine("Texts", name, "Phrases.txt");
            var absPhrasesPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                phrasesPath);
            if (!File.Exists(absPhrasesPath))
                throw new FileNotFoundException();
            var lines = File.ReadAllLines(absPhrasesPath);
            foreach (var line in lines.Select(s => s.Split(mainSeparator)))
                phrases[line[0]] = line[1].Split(phrasesSeparator).ToList();
        }
    }

    public class PlayerTable: TableLayoutPanel
    {
        public PlayerTable(Label portait, Panel scales, PhrasesBox phrasesBox)
        {
            RowStyles.Add(
                new RowStyle(SizeType.Percent, 60f));
            RowStyles.Add(
                new RowStyle(SizeType.Percent, 10f));
            RowStyles.Add(
                new RowStyle(SizeType.Percent, 30f));
            ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 100f));

            Controls.Add(portait, 0, 0);
            Controls.Add(scales, 0, 1);
            Controls.Add(phrasesBox, 0, 2);
            Dock = DockStyle.Fill;
        }
    }
}
