﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeAbreast
{
    public class Match
    {
        public List<BoardItem> Line;
        public Direction Dir;
        public int Length { get { return Line.Count;  } }
        public BoardItem Start { get { return Line.First(); } }
        public bool IsSuccessful { get { return Length > 2; } }

        public Match(BoardItem startItem, Direction direction)
        {
            Line = new List<BoardItem>() { startItem };
            Dir = direction;
        }

        public void Append(BoardItem item)
        {
            Line.Add(item);
        }

    }
}
