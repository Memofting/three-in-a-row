﻿using System; 
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeAbreast
{  
    public interface IBoard
    {
        void SwapElements(SwapAction swapAction);
        bool IsInBorders(Point pnt);

        SwapAction FindFirstPossibleSwap();
        List<SwapAction> FindAllPossibleSwaps();

        SwapAction TakeHint();
        void MakeGameSteps();
    }
}
