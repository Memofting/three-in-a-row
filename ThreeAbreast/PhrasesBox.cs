﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    public class PhrasesBox : TextBox
    {
        public PhrasesBox()
        {
            ReadOnly = true;
            TabStop = false;
            Font = new Font("Times new roman", 20);
            BackColor = Color.MediumVioletRed;
            ForeColor = Color.Black;
            Multiline = true;
            WordWrap = true;
            Dock = DockStyle.Fill;
        }
    }
}
