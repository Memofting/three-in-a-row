﻿using NUnit.Framework;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

namespace ThreeAbreast
{
    [TestFixture]
    public class BoardTest
    {
        private static Dictionary<string, string> FilledBoards = new Dictionary<string, string>
        {
            { "swapping_move" , @"
3x3
C B C
D C D
C B C"
            },
            { "unswapping_move", @"
2x2
B C
C D"
            },
            { "only_left_swap", @"
3x3
B D C
A B A
B C C"      },
            { "with_match_in_line", @"
1x3
C C C"      },
            { "with_match_in_column", @"
3x1
C
C
C"          },
            { "swapping_field", @"
5x5
D C C D B
A B C B A
D D B A C
A B C D A
D C C D A"  }
        };
        private static Dictionary<string, string> BoardTypes = new Dictionary<string, string>
        {
            { "A", @"
1x1
A"},
            { "B", @"
1x1
B"},
            { "C", @"
1x1
C"},
            { "D", @"
1x1
D"},
            { "Empty", @"
1x1
E"},
            { "Blocked", @"
1x1
#"}
        };
        private static Dictionary<string, (string, Direction, BoardItem)> SwapCases = 
            new Dictionary<string, (string, Direction, BoardItem)>
        {
            { "swap up", (@"
1x1
A", Direction.Up, new BoardItem(Point.Empty, ItemType.A)) },
            { "swap down", (@"
2x1
A
B", Direction.Down, new BoardItem(Point.Empty, ItemType.B))},
            { "swap left", (@"
1x1
A", Direction.Left, new BoardItem(Point.Empty, ItemType.A))},
            { "swap right", (@"
1x2
A B
", Direction.Right, new BoardItem(Point.Empty, ItemType.B))}
        };
        private static Dictionary<string, (string, int, int, ItemType)> GameStepsCases =
            new Dictionary<string, (string, int, int, ItemType)>
        {
            { "vertical", (@"
4x1
B
A
A
A", 0, 3, ItemType.B)},
            { "horizontal", (@"
2x3
B # #
A A A", 0, 1, ItemType.B)}
        };

        [TestCase("A")]
        [TestCase("B")]
        [TestCase("C")]
        [TestCase("D")]
        [TestCase("Empty")]
        [TestCase("Blocked")]
        public void TestBoardCreation(string boardType)
        {
            var board = BoardLoader.MakeBoard(BoardTypes[boardType]);
            var boardInfoLines = BoardLoader.PrepareBoardInfo(BoardTypes[boardType]);
            var header = boardInfoLines[0].Split(BoardLoader.Separators);
            var assumedHeight = Int32.Parse(header[0]);
            var assumedWidth = Int32.Parse(header[1]);
            var expectedType = BoardItem.IconToType[boardInfoLines[1]];

            Assert.AreEqual(assumedHeight, board.Height);
            Assert.AreEqual(assumedWidth, board.Width);
            Assert.AreEqual(expectedType, board[0, 0].Type);
        }

        [TestCase("swapping_move", Direction.Up, ExpectedResult = true)]
        [TestCase("swapping_move", Direction.Down, ExpectedResult = true)]
        [TestCase("swapping_move", Direction.Left, ExpectedResult = true)]
        [TestCase("swapping_move", Direction.Right, ExpectedResult = true)]
        [TestCase("unswapping_move", Direction.Up, ExpectedResult = false)]
        [TestCase("unswapping_move", Direction.Down, ExpectedResult = false)]
        [TestCase("unswapping_move", Direction.Left, ExpectedResult = false)]
        [TestCase("unswapping_move", Direction.Right, ExpectedResult = false)]
        [TestCase("only_left_swap", Direction.Left, ExpectedResult = true)]
        [TestCase("only_left_swap", Direction.Right, ExpectedResult = false)]
        public bool TestIsPossibleSwap(string mapName, Direction direction)
        {
            var board = BoardLoader.MakeBoard(BoardTest.FilledBoards[mapName]);
            var swappedElement = board.GetAdjacentItem(
                board[1, 1], direction);
            if (swappedElement == null)
                return false;
            var assumedSwap = new SwapAction(
                board[1, 1], swappedElement);
            return board.IsPossibleSwap(assumedSwap);
        }

        [TestCase("swap down")]
        [TestCase("swap up")]
        [TestCase("swap left")]
        [TestCase("swap right")]
        public void TestSwapCases(string swapCaseName)
        {
            var (map, direction, expectedBoardItem) = SwapCases[swapCaseName];
            var board = BoardLoader.MakeBoard(map);
            var adjCoord = Board.GetShiftedPoint(direction, Point.Empty);
            board.SwapElements(board[Point.Empty], board[adjCoord]);
            Assert.AreEqual(expectedBoardItem, board[Point.Empty]);
        }

        [TestCase("swapping_move")]
        [TestCase("with_match_in_line")]
        [TestCase("with_match_in_column")]
        public void TestExistsFirstMatchCases(string mapName)
        {
            var board = BoardLoader.MakeBoard(BoardTest.FilledBoards[mapName]);
            Assert.IsNotNull(board.FindFirstPossibleSwap(), "Incorrect finding first match cases.");
        }

        [TestCase("unswapping_move")]
        public void TestNonExistFirstMatchCases(string mapName)
        {
            var board = BoardLoader.MakeBoard(BoardTest.FilledBoards[mapName]);
            Assert.IsNull(board.FindFirstPossibleSwap(),
                "First match combination should be null");
        }

        [Test]
        public void TestEqualSwapCases()
        {
            var board = BoardLoader.MakeBoard(BoardTest.FilledBoards["swapping_move"]);

            var swapDown = new SwapAction(board[1, 1], board[2, 1]);
            var swapUp = new SwapAction(board[2, 1], board[1, 1]);
            var swapRight = new SwapAction(board[1, 1], board[1, 2]);
            var swapLeft = new SwapAction(board[1, 2], board[1, 1]);


            Assert.AreEqual(swapDown, swapUp,
                "Incorrect comparing between swap combinations (up/down)");
            Assert.AreEqual(swapLeft, swapRight,
                "Incorrect comparing between swap combinations (left/right)");
        }

        [TestCase("vertical")]
        [TestCase("horizontal")]
        public void TestGameStepsCases(string gameStepCaseName)
        {
            var (map, x, y, type) = GameStepsCases[gameStepCaseName];
            var board = BoardLoader.MakeBoard(map);
            board.MakeGameSteps();
            Assert.AreEqual(type, board[x, y].Type, 
                "Incorrect type of board item after game steps");

        }
    }
}
