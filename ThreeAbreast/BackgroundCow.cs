﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    public class BackgroundCow : Form
    {
        public BackgroundCow()
        {
            Dock = DockStyle.Fill;
            Controls.Add(new CowPanel());
        }
    }

    public class CowPanel : Panel
    {
        public CowPanel()
        {
            Dock = DockStyle.Fill;
            var absBoardPath = Path.Combine(Path.GetDirectoryName(
                    Assembly.GetExecutingAssembly().Location),
                    Path.Combine("Images", "BoardBackground", "BackgroundCow.jpg"));
            var image = Image.FromFile(absBoardPath);
            var scaledImage = (Image)new Bitmap(image, new Size(image.Width / 8, image.Height / 8));
            var graphics = CreateGraphics();
            BackgroundImage = scaledImage;
            var label = new Label();
            label.Text = "Button";
            label.ForeColor = Color.White;
            //label.Parent = this;
            label.BackColor = Color.Transparent;
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            Controls.Add(label);
        }
    }
}
