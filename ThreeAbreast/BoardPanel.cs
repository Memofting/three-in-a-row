﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreeAbreast
{
    public class BoardPanel : Panel
    {
        private HeaderTable headerTable;
        private BoardTable boardTable;
        private MainTable mainTable;
        public string ScrollAccount { set { headerTable.scoreAccount.Text = value; } }

        public BoardPanel(
                Board board, Queue<Action> animationEventQueue,
                Queue<BoardItem> swapItemsQueue)
        {
            boardTable = new BoardTable(
                board, animationEventQueue, swapItemsQueue);
            headerTable = new HeaderTable();
            mainTable = new MainTable(headerTable, boardTable);
            Dock = DockStyle.Fill;

            var absBoardPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                Path.Combine("Images", "BoardBackground", "NyanCat.png"));
            var image = Image.FromFile(absBoardPath);
            var scaledImage = (Image)new Bitmap(image, new Size(image.Width / 2, image.Height / 2));
            var graphics = CreateGraphics();
            BackgroundImage = scaledImage;

            Controls.Add(mainTable);
        }

        public Label this[int x, int y]
        {
            get { return (Label)boardTable.GetControlFromPosition(x, y); }
        }
    }

    public class HeaderTable : TableLayoutPanel
    {
        public Label scoreInfo;
        public Label scoreAccount;

        public HeaderTable()
        {

            scoreAccount = new Label();
            scoreAccount.Text = String.Format("{0}:{1}", 0, 0);
            scoreAccount.Font = new Font("Time new roman", 30);
            scoreAccount.ForeColor = Color.AntiqueWhite;
            scoreAccount.Dock = DockStyle.Fill;
            scoreAccount.TextAlign = ContentAlignment.MiddleCenter;
            scoreAccount.ForeColor = Color.White;

            RowStyles.Add(
                new RowStyle(SizeType.Percent, 100f));
            ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 100f));
            Controls.Add(scoreAccount, 0, 0);
            Dock = DockStyle.Fill;
            BackColor = Color.Transparent;
        }
    }

    public class MainTable : TableLayoutPanel
    {
        public MainTable(HeaderTable headerTable, BoardTable boardTable)
        {
            ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 100f));
            RowStyles.Add(
                new RowStyle(SizeType.Percent, 10f));
            RowStyles.Add(
                new RowStyle(SizeType.Percent, 90f));
            Controls.Add(headerTable, 0, 0);
            Controls.Add(boardTable, 0, 1);
            Dock = DockStyle.Fill;
            BackColor = Color.Transparent;
        }
    }

    public class BoardTable : TableLayoutPanel
    {
        public BoardTable(
            Board board, Queue<Action> animationEventQueue,
            Queue<BoardItem> swapItemsQueue)
        {

            for (var i = 0; i < board.Width; i++)
                ColumnStyles.Add(
                    new ColumnStyle(SizeType.Percent, 100f / board.Width));
            for (var j = 0; j < board.Height; j++)
                RowStyles.Add(
                    new RowStyle(SizeType.Percent, 100f / board.Height));

            for (int column = 0; column < board.Width; column++)
                for (int row = 0; row < board.Height; row++)
                {
                    var iRow = row;
                    var iColumn = column;
                    var label = new Label();
                    label.Dock = DockStyle.Fill;
                    label.Margin = new Padding(3);
                    label.BackColor = Color.Transparent;
                    label.BackgroundImageLayout = ImageLayout.Stretch;
                    label.Click += (sender, args) =>
                    {
                        if (sender is Label && animationEventQueue.Count == 0)
                        {
                            var lab = (Label)sender;
                            var pos = GetPositionFromControl(lab);
                            var item = board[pos.Column, pos.Row];
                            swapItemsQueue.Enqueue(item);
                        }
                    };
                    Controls.Add(label, iColumn, iRow);
                }

            Dock = DockStyle.Fill;
            BackColor = Color.Transparent;
        }
    }
}
