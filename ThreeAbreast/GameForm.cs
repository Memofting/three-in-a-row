﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace ThreeAbreast
{
    public class GameForm : Form
    {

        public WMPLib.WindowsMediaPlayer Player;
        private bool isPlayer1Turn = true;
        private bool isPlayerSwapOccured = false;
        private int player1ScoreCount = 0;
        private int player2ScoreCount = 0;
        private const int scoreCount = 10;

        private Queue<Action> animationEventQueue = new Queue<Action>();
        private Queue<BoardItem> swapItemsQueue = new Queue<BoardItem>();
        private Queue<string> scoreQueue = new Queue<string>();
        private Queue<BoardItem[,]> boardsQueue = new Queue<BoardItem[,]>();


        public GameForm(Board board)
        {
            var animationTimer = new System.Windows.Forms.Timer { Interval = 100 };
            var swapTimer = new System.Windows.Forms.Timer { Interval = 20 };
            var scoreTimer = new System.Windows.Forms.Timer { Interval = 20 };

            var boardPanel = new BoardPanel(
                board, this.animationEventQueue, this.swapItemsQueue);
            var firstPlayerPanel = new PlayerPanelPlus("MainCharacter");
            var secondPlayerPanel = new PlayerPanelPlus("FirstEnemy");
            animationTimer.Tick += firstPlayerPanel.TimerTick;
            animationTimer.Tick += secondPlayerPanel.TimerTick;

            var table = new TableLayoutPanel();
            table.RowStyles.Add(
                new RowStyle(SizeType.Percent, 100f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 20f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 60f));
            table.ColumnStyles.Add(
                new ColumnStyle(SizeType.Percent, 20f));

            table.Controls.Add(firstPlayerPanel, 0, 0);
            table.Controls.Add(boardPanel, 1, 0);
            table.Controls.Add(secondPlayerPanel, 2, 0);
            table.Dock = DockStyle.Fill;

            Controls.Add(table);

            board.StateChanged += (boardItem) =>
            {
                animationEventQueue.Enqueue(
                    () => boardPanel[boardItem.X, boardItem.Y]
                            .BackgroundImage = boardItem.BackImage);
            };
            board.BoardChanged += (boardArray) =>
            {
                animationEventQueue.Enqueue(
                    () =>
                    {
                        for (var y = 0; y < boardArray.GetLength(0); y++)
                            for (var x = 0; x < boardArray.GetLength(1); x++)
                            {
                                boardPanel[y, x].BackgroundImage = boardArray[y, x].BackImage;
                            }
                    });
            };
            board.AccumulatedScore += (AScore, BScore, CScore, DScore) =>
            {
                var sum = AScore + BScore + CScore + DScore;
                if (isPlayer1Turn)
                {
                    player1ScoreCount += sum;
                    firstPlayerPanel.SetMessageFromScore(
                        AScore, BScore, CScore, DScore);
                }
                else
                {
                    player2ScoreCount += sum;
                    secondPlayerPanel.SetMessageFromScore(
                        AScore, BScore, CScore, DScore);
                }
                scoreQueue.Enqueue(String.Format(
                    "{0}:{1}", player1ScoreCount, player2ScoreCount));
            };
            animationTimer.Tick += (sender, args) =>
            {
                if (animationEventQueue.Count > 0)
                    animationEventQueue.Dequeue()();
                else if (swapItemsQueue.Count == 0 && isPlayerSwapOccured && !isPlayer1Turn)
                {
                    // выход мегаИИ
                    board.SwapElements(board.FindFirstPossibleSwap());
                    board.MakeGameSteps();
                    isPlayerSwapOccured = false;
                    isPlayer1Turn = !isPlayer1Turn;
                }
                Invalidate();
            };
            swapTimer.Tick += (sender, args) =>
            {
                if (swapItemsQueue.Count == 2 &&
                    animationEventQueue.Count == 0)
                {
                    var item1 = swapItemsQueue.Dequeue();
                    var item2 = swapItemsQueue.Dequeue();
                    var swapAction = new SwapAction(item1, item2);
                    if (board.IsPossibleSwap(swapAction))
                        board.SwapElements(item1, item2, true);
                    board.MakeGameSteps();
                    isPlayerSwapOccured = true;
                    isPlayer1Turn = !isPlayer1Turn;
                }
            };
            scoreTimer.Tick += (sender, args) =>
            {
                if (animationEventQueue.Count == 0 && scoreQueue.Count != 0)
                    boardPanel.ScrollAccount = scoreQueue.Dequeue();
                if (player1ScoreCount > scoreCount || 
                    player2ScoreCount > scoreCount)
                {
                    scoreTimer.Stop();
                    if (player1ScoreCount > player2ScoreCount)
                        ShowText(new Size(320, 240), "Victory");
                    else
                        ShowText(new Size(320, 240), "Defeat");
                    this.Close();
                }
            };
            Dock = DockStyle.Fill;
            WindowState = FormWindowState.Maximized;

            ShowText(new Size(640, 380), "Introduce");

            PlayFile();
            board.Start();
            animationTimer.Start();
            swapTimer.Start();
            scoreTimer.Start();
        }

        private void ShowText(Size size, string location)
        {
            var dialog = new ModalInfoForm(size, location);
            dialog.ShowDialog(this);
            dialog.Dispose();
        }

        private Image GetBackgroundImage()
        {
            var backgroundPath = Path.Combine("Images", "PlayerBackgrounds", "MainCharacter", "background.png");
            var absBackgroundPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                backgroundPath);
            return Image.FromFile(absBackgroundPath);
        }

        private void PlayFile()
        {
            var backMusicPath = Path.Combine("Sound", "BackgroundMusic.wma");
            var absBackMusicPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                backMusicPath);
            Player = new WMPLib.WindowsMediaPlayer();
            Player.URL = absBackMusicPath;
            Player.settings.setMode("loop", true);
            Player.controls.play();
        }
    }
}
