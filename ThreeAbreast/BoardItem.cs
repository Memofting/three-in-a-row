﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace ThreeAbreast
{
    public enum ItemType
    { Blocked = -2, Empty = -1, A = 0, B, C, D}

    public class BoardItem
    {
        public readonly Size ImageIconSize = new Size(64, 64); 
        static private Random randomIntGenerator = new Random();
        static private List<ItemType> validTypes = 
            Enum.GetValues(typeof(ItemType))
            .Cast<ItemType>()
            .Where(x => x > ItemType.Empty)
            .ToList();
        static public Dictionary<string, ItemType> IconToType = 
            new Dictionary<string, ItemType>
       {
            { "#", ItemType.Blocked },
            { "E", ItemType.Empty },
            { "A", ItemType.A },
            { "B", ItemType.B },
            { "C", ItemType.C },
            { "D", ItemType.D }
        };
        static private Dictionary<ItemType, Color> TypeToColor =
            new Dictionary<ItemType, Color>
            {
                { ItemType.Blocked, Color.White},
                { ItemType.Empty, Color.Black},
                { ItemType.A, Color.Red},
                { ItemType.B, Color.Blue},
                { ItemType.C, Color.Green},
                { ItemType.D, Color.Yellow}
            };

        static private Dictionary<ItemType, Image> TypeToImage =
            new Dictionary<ItemType, Image>
            {
                { ItemType.Blocked, TypeToScaledImage(ItemType.Blocked) },
                { ItemType.Empty, TypeToScaledImage(ItemType.Empty) },
                { ItemType.A, TypeToScaledImage(ItemType.A) },
                { ItemType.B, TypeToScaledImage(ItemType.B) },
                { ItemType.C, TypeToScaledImage(ItemType.C) },
                { ItemType.D, TypeToScaledImage(ItemType.D) }
            };

        static public Dictionary<ItemType, string> TypeToIcon = IconToType
            .ToDictionary(pair => pair.Value, pair => pair.Key);

        public Point Coords = new Point();
        public int X { get { return Coords.X; } }
        public int Y { get { return Coords.Y; } }
        public ItemType Type { get; set; }
        public Color BackColor { get { return TypeToColor[Type]; } }
        public Image BackImage { get { return TypeToImage[Type]; } }
        public bool IsEmpty { get { return Type == ItemType.Empty; } }

        public BoardItem(Point coords, ItemType type)
        {
            Coords = new Point(coords.X, coords.Y);
            Type = type;
        }

        public BoardItem(BoardItem other)
        {
            Coords = new Point(other.X, other.Y);
            Type = other.Type;
        }

        public void Swap(BoardItem other)
        {
            var temp = Coords;
            Coords = other.Coords;
            other.Coords = temp;
        }

        static public ItemType GetRandomValidItemType()
        {
            var i = randomIntGenerator.Next(validTypes.Count);
            return validTypes[i];
        }

        public override bool Equals(object obj)
        {
            if (!(obj is BoardItem)) return false;
            var bi = (BoardItem)obj;
            return Type == bi.Type;
        }

        public override int GetHashCode()
        {
            return Coords.X + 307 * Coords.Y + 307 * 307 * (int)Type;
        }

        public override string ToString()
        {
            return TypeToIcon[Type];
        }

        static private Image TypeToScaledImage(ItemType item)
        {
            var pathToImagesFolder = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                Path.Combine("Images", "BoardItems"));
            string itemImageName = "blackChest.png";
            switch (item)
            {
                case ItemType.A:
                    itemImageName = "blue.png";
                    break;
                case ItemType.B:
                    itemImageName = "red.png";
                    break;
                case ItemType.C:
                    itemImageName = "green.png";
                    break;
                case ItemType.D:
                    itemImageName = "purple.png";
                    break;
            }
            var absImagePath = Path.Combine(pathToImagesFolder, itemImageName);
            return new Bitmap(Image.FromFile(absImagePath), 64, 64);

        }
    }
}
