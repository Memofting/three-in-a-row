﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreeAbreast
{
    public enum Direction
    {
        Up, Down, Right, Left
    }

    public class SwapAction
    {
        public List<BoardItem> BoardItems { get; }
        public BoardItem FirstItem { get { return BoardItems[0]; } }
        public BoardItem SecondItem { get { return BoardItems[1]; } }

        public SwapAction(BoardItem boardItem1, BoardItem boardItem2)
        {
            BoardItems = new List<BoardItem> { boardItem1, boardItem2 };
            if (boardItem2.Coords.Y < boardItem1.Coords.Y || 
                boardItem2.Coords.X < boardItem1.Coords.X)
                BoardItems.Reverse();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SwapAction)) return false;
            var mc = (SwapAction)obj;

            return 
                (FirstItem == mc.FirstItem && 
                SecondItem == mc.SecondItem) ||
                (FirstItem == mc.SecondItem && 
                SecondItem == mc.FirstItem);
        }

        public override int GetHashCode()
        {
            return SecondItem.GetHashCode() + 307 * FirstItem.GetHashCode();
        }
    }
}
