﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace ThreeAbreast
{
    public class ModalInfoForm : Form
    {
        public ModalInfoForm(Size targetSize, string folderName)
        {
            var text = GetIntroduce(folderName);
            var textBox = new PhrasesBox();
            textBox.BackColor = Color.White;
            textBox.Text = text;
            StartPosition = FormStartPosition.CenterScreen;
            Dock = DockStyle.Fill;
            ClientSize = targetSize;
            Controls.Add(textBox);
        }

        private string GetIntroduce(string folderName)
        {
            var pathToText = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location),
                Path.Combine("Texts", folderName, "text.txt"));
            return File.ReadAllText(pathToText);
        }
    }
}
