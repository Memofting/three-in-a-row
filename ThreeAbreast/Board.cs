﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace ThreeAbreast
{
    public class Board : IBoard
    {
        static private Point emptyPoint = Point.Empty;
        public int Height { get; }
        public int Width { get; }
        private BoardItem[,] MainBoard { get; set; }
        public event Action<BoardItem> StateChanged;
        public event Action<BoardItem[,]> BoardChanged;
        public event Action<int, int, int, int> AccumulatedScore;

        public Board(int height, int width)
        {
            Height = height;
            Width = width;
            MainBoard = new BoardItem[Width, Height];
        }

        public Board(BoardItem[,] shape)
        {
            Height = shape.GetLength(0);
            Width = shape.GetLength(1);
            MainBoard = (BoardItem[,])shape.Clone();
        }

        public void Start()
        {
            //for (var x = 0; x < Width; x++)
            //    for (var y = Height - 1; y >= 0; y--)
            //        StateChanged?.Invoke(new BoardItem(this[x, y]));
            BoardChanged?.Invoke((BoardItem[,])MainBoard.Clone());
        }

        public void SwapElements(BoardItem boardItem1, BoardItem boardItem2, bool isStateChanged = false)
        {
            if (boardItem1 != null && boardItem2 != null &&
                !boardItem1.IsEmpty && !boardItem2.IsEmpty)
            {
                MainBoard[boardItem1.Coords.X, boardItem1.Coords.Y] = boardItem2;
                MainBoard[boardItem2.Coords.X, boardItem2.Coords.Y] = boardItem1;
                boardItem1.Swap(boardItem2);
                if (isStateChanged)
                {
                    StateChanged?.Invoke(new BoardItem(boardItem1));
                    StateChanged?.Invoke(new BoardItem(boardItem2));
                }
            }
        }

        public void SwapElements(SwapAction swap)
        {
            SwapElements(swap.FirstItem, swap.SecondItem);
        }

        public void SwapElements(int x1, int y1, int x2, int y2)
        {
            SwapElements(MainBoard[x1, y1], MainBoard[x2, y2]);
        }

        public IEnumerable<Match> FindMatchInArea(int left, int up, int right, int bottom)
        {
            left = Math.Max(0, left);
            up = Math.Max(0, up);
            right = Math.Min(Width, right);
            bottom = Math.Min(Height, bottom);

            for (var y = up; y < bottom; y++)
                for (var x = left; x < right;)
                {
                    var centerItem = MainBoard[x, y];
                    var match = GetMatchFromPoint(centerItem, Direction.Right);
                    if (match.IsSuccessful && IsInRange(x + 2, left, right))
                        yield return match;
                    x += match.Length;
                }
            for (var x = left; x < right; x++)
                for (var y = up; y < bottom;)
                {
                    var centerItem = MainBoard[x, y];
                    var match = GetMatchFromPoint(centerItem, Direction.Down);
                    if (match.IsSuccessful && IsInRange(y + 2, up, bottom))
                        yield return match;
                    y += match.Length;
                }
        }

        public IEnumerable<SwapAction> FindSwapsInArea(int left, int up, int right, int bottom)
        {
            left = Math.Max(0, left);
            up = Math.Max(0, up);
            right = Math.Min(Width, right);
            bottom = Math.Min(Height, bottom);
            for (var y = up; y < bottom; y++)
                for (var x = left; x < right; x++)
                {
                    var centerItem = MainBoard[x, y];

                    var adjacentItems = new List<BoardItem>();

                    if (IsInBorders(centerItem, Direction.Right))
                        adjacentItems.Add(MainBoard[x + 1, y]);
                    if (IsInBorders(centerItem, Direction.Down))
                        adjacentItems.Add(MainBoard[x, y + 1]);
                    foreach (var adjacentItem in adjacentItems)
                    {
                        var swap = new SwapAction(centerItem, adjacentItem);
                        var matchResult = IsPossibleSwap(swap);

                        if (matchResult)
                            yield return swap;
                    }
                }
        }

        public Match GetMatchFromPoint(Point pnt, Direction direction)
        {
            /*
             * Подразумевается, что match "выходит" из этой точки, то есть,
             * если у нас есть поле:
             * 000
             * 000
             * 000
             * то из точки (1,1) мы найдём лишь match длины 2
             */
            if (!IsInBorders(pnt, Width, Height))
                return null;
            Match match = new Match(this[pnt], direction);
            while (true)
            {
                pnt = GetShiftedPoint(direction, pnt);
                if (!IsInBorders(pnt, Width, Height))
                    break;
                var item = this[pnt];
                if (item.Type != match.Start.Type)
                    break;
                match.Append(item);
            }
            return match;
        }

        public Match GetMatchFromPoint(BoardItem item, Direction direction)
        {
            return GetMatchFromPoint(item.Coords, direction);
        }

        public Match FindFirstPossibleMatch()
        {
            return FindMatchInArea(0, 0, Width, Height).FirstOrDefault();
        }

        public List<Match> FindAllPossibleMatches()
        {
            return FindMatchInArea(0, 0, Width, Height).ToList();
        }

        public bool HasMatchesInArea(int left, int up, int right, int bottom)
        {
            return FindMatchInArea(left, up, right, bottom).FirstOrDefault() != null;
        }

        public SwapAction FindFirstPossibleSwap()
        {
            return FindSwapsInArea(0, 0, Width, Height).FirstOrDefault();
        }

        public List<SwapAction> FindAllPossibleSwaps()
        {
            return FindSwapsInArea(0, 0, Width, Height).ToList();
        }

        // В приниципий можно ограничить область проверки (но зачем?!)
        // чтобы не пробегать все поле, ради одной проверки, либо сделать
        // поле с текущими match-ами на поле статическим, чтобы считать
        // его один раз за игровой цикл.
        public bool IsPossibleSwap(SwapAction swap)
        {
            if (!IsAdjacentItem(swap.FirstItem, swap.SecondItem))
                return false;
            SwapElements(swap);
            var matchResult = HasMatchesInArea(0, 0, Width, Height);
            SwapElements(swap);

            return matchResult;
        }

        static private bool IsInRange(int value, int from, int to)
        {
            return from <= value && value < to;
        }

        public bool IsInBorders(Point pnt, int xBorder, int yBorder)
        {
            return IsInRange(pnt.X, 0, xBorder) &&
                   IsInRange(pnt.Y, 0, yBorder);
        }

        public bool IsInBorders(Point pnt)
        {
            return IsInBorders(pnt, Width, Height);
        }

        public bool IsInBorders(Point pnt, Direction direction)
        {
            return IsInBorders(GetShiftedPoint(direction, pnt));
        }

        public bool IsInBorders(BoardItem item, Direction direction)
        {
            return IsInBorders(item.Coords, direction);
        }

        public BoardItem GetAdjacentItem(BoardItem item, Direction direction)
        {
            var newCoords = GetShiftedPoint(direction, item.Coords);
            if (!IsInBorders(newCoords))
                return null;
            return this[newCoords];
        }

        private bool IsAdjacentItem(BoardItem from, BoardItem to)
        {
            return Math.Abs(from.X - to.X) + Math.Abs(from.Y - to.Y) == 1;
        }

        public SwapAction TakeHint()
        {
            throw new NotImplementedException();
        }

        static public Point GetShiftedPoint(Direction direction, Point point)
        {

            var dx = 0;
            var dy = 0;
            if (direction == Direction.Right)
                dx = +1;
            else if (direction == Direction.Down)
                dy = +1;
            else if (direction == Direction.Up)
                dy = -1;
            else if (direction == Direction.Left)
                dx = -1;

            return new Point(point.X + dx, point.Y + dy);

        }

        public override string ToString()
        {
            var view = new StringBuilder();
            var line = new StringBuilder();
            var del = "  ";
            for (var j = 0; j < Height; j++)
            {
                line.Clear();
                for (var i = 0; i < Width; i++)
                {
                    string item = " ";
                    if (!this[i, j].IsEmpty)
                        item = this[i, j].ToString();
                    line.AppendFormat("{1,6}{0}", item, del);
                }
                view.AppendLine(line.ToString());
            }
            return view.ToString();
        }

        public void PrintBoard()
        {
            Console.CursorLeft = 0;
            Console.CursorTop = 0;
            Console.WriteLine(this);
            Console.ReadKey();
        }

        // возвращать карты
        public void MakeGameSteps()
        {
            // не рассчитан на поля с блоками
            while (HasMatchesInArea(0, 0, Width, Height))
            {
                var (AScore, BScore, CScore, DScore) = DeleteMatches();
                MainBoard = GetShiftedBoard().DefaultIfEmpty(MainBoard).Last();
                AccumulatedScore?.Invoke(AScore, BScore, CScore, DScore);
                BoardChanged?.Invoke((BoardItem[,])MainBoard.Clone());
            }
        }

        private IEnumerable<Queue<ItemType>> GetColumns()
        {
            for (var x = 0; x < Width; x++)
            {
                var column = new Queue<ItemType>();
                for (var y = Height - 1; y >= 0; y--)
                {
                    if (!this[x, y].IsEmpty)
                        column.Enqueue(this[x, y].Type);
                }
                yield return column;
            }
        }

        private void SetColumns(IEnumerable<Queue<ItemType>> columns)
        {
            var x = -1;
            foreach (var column in columns)
            {
                x++;
                if (column.Count == Height)
                    continue;
                if (column.Count < Height - 1)
                    for (var topY = 0; topY < column.Count; topY++)
                        StateChanged?.Invoke(new BoardItem(new Point(x, topY), ItemType.Empty));
                for (var y = Height - 1; y >= 0; y--)
                {
                    if (column.Count > 0)
                        this[x, y].Type = column.Dequeue();
                    else
                        this[x, y].Type = BoardItem.GetRandomValidItemType();
                    StateChanged?.Invoke(new BoardItem(this[x, y]));
                }
            }

        }

        private IEnumerable<BoardItem[,]> GetShiftedBoard()
        {
            for (var h = 0; h < Height; h++)
            {
                var oldMap = this.ToString();
                var oldArray = (BoardItem[,])MainBoard.Clone();
                for (var j = Height - 1; j >= 0; j--)
                {
                    for (var i = 0; i < Width; i++)
                    {
                        // условие появления новых клеток
                        if (oldArray[i, j].IsEmpty && j == 0)
                            oldArray[i, j].Type = BoardItem.GetRandomValidItemType();
                        var downPoint = new Point(i, j - 1);
                        if (IsInBorders(downPoint) && oldArray[i, j].IsEmpty && !oldArray[i, j - 1].IsEmpty)
                        {
                            oldArray[i, j].Type = oldArray[i, j - 1].Type;
                            oldArray[i, j - 1].Type = ItemType.Empty;
                        }
                    }
                    yield return (BoardItem[,])oldArray.Clone();
                }

                // отсечение перебора
                if (oldMap != this.ToString())
                    continue;
                yield break;
            }
            

        }

        private (int, int, int, int) DeleteMatches()
        {
            var AScore = 0;
            var BScore = 0;
            var CScore = 0;
            var DScore = 0;

            foreach (var item in FindAllPossibleMatches()
                                    .SelectMany(match => match.Line)
                                    .OrderBy(item => item.Y * Width + item.X))
            {
                switch (item.Type)
                {
                    case ItemType.A:
                        AScore++;
                        break;
                    case ItemType.B:
                        BScore++;
                        break;
                    case ItemType.C:
                        CScore++;
                        break;
                    case ItemType.D:
                        DScore++;
                        break;
                }
                item.Type = ItemType.Empty;
                StateChanged?.Invoke(new BoardItem(item));
            }
            return (AScore, BScore, CScore, DScore);
        }

        //Можно ли индексаторы друг от дружки наследовать или ещё что-нить такое?
        public BoardItem this[int x, int y]
        {
            get { return this[new Point(x, y)]; }
            set { this[new Point(x, y)] = value; }
        }
        public BoardItem this[Point pnt, int dx, int dy]
        {
            get { return this[new Point(pnt.X + dx, pnt.Y + dy)]; }
            set { this[new Point(pnt.X + dx, pnt.Y + dy)] = value; }
        }
        public BoardItem this[Point pnt]
        {
            get { return IsInBorders(pnt) ? MainBoard[pnt.X, pnt.Y] : null; }
            set {
                if (!IsInBorders(pnt))
                    throw new IndexOutOfRangeException();
                MainBoard[pnt.X, pnt.Y] = value;
            }
        }
    }
}
